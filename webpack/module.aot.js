'use strict';

let commonRules = require('./rules.common');

module.exports = {
  rules: [
    {
      test: /\.ts$/,
      use: ['@ngtools/webpack', 'angular2-router-loader']
    }
  ].concat(commonRules)
};

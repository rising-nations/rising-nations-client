'use strict';

let webpack = require('webpack');
let CopyWebpackPlugin = require('copy-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
let path = require('path');

module.exports = [
  new webpack.ProgressPlugin(),
  new webpack.ContextReplacementPlugin(
    // The (\\|\/) piece accounts for path separators in *nix and Windows
    /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
    path.join(process.cwd(), 'src')
  ),
  new webpack.optimize.CommonsChunkPlugin({
    name: ['vendor', 'polyfills']
  }),
  new CopyWebpackPlugin([{
    from: path.join(process.cwd(), 'src', 'public')
  }]),
  new HtmlWebpackPlugin({
    publicPath: './',
    template: 'public/index.html',
    chunksSortMode: 'dependency'
  })
];

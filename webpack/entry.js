'use strict';

module.exports = {
  'polyfills': './app/polyfills.ts',
  'main': './app/main.ts'
};

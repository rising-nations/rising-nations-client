'use strict';

let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [
  {
    test: /\.(scss|sass)$/,
    include: path.resolve(process.cwd(), 'src', 'app'),
    loaders: ['to-string-loader', 'css-loader', 'sass-loader']
  },
  {
    test: /\.css$/,
    exclude: path.resolve(process.cwd(), 'src', 'app'),
    loader: ExtractTextPlugin.extract({
      fallbackLoader: 'style-loader',
      loader: 'css-loader'
    })
  },
  // support for .html as raw text
  // todo: change the loader to something that adds a hash to images
  {
    test: /\.html$/,
    loaders: ['raw-loader']
  },

  {
    test: /\.glsl$/,
    loaders: ['raw-loader']
  },
];

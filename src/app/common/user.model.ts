export interface User {
    username: string;
    email: string;
    password: string;
    agbAccepted;
    newsletterSubscribed;
}
import { User } from './user.model';
import { Observable, Observer } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

const SERVER_URL = 'http://localhost:8080/rising-nations-server';

@Injectable()
export class AuthService {
    private constructor(private http: Http) { }

    public static LOCAL_STORAGE_AUTH_TOKEN_KEY = "token";

    getSessionToken() : string {
        return window.localStorage.getItem(AuthService.LOCAL_STORAGE_AUTH_TOKEN_KEY);
    }

    clearSessionToken(){
        window.localStorage.removeItem(AuthService.LOCAL_STORAGE_AUTH_TOKEN_KEY);
    }

    login(username: string, password: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            let data = {
                "username": username,
                "password": password
            };
            this.http.post(SERVER_URL + '/auth/login', data).subscribe(
                (response: any) => {
                    let token = response.json().token;
                    window.localStorage.setItem(AuthService.LOCAL_STORAGE_AUTH_TOKEN_KEY, token);
                    observer.next(token);
                    observer.complete();
                },
                (response: any) => {
                    observer.error(response.json().errors);
                    observer.complete();
                }
            );
        });
    }
    checkLoginState() {
        return new Observable((observer: Observer<any>) => {
            let data = {
                "token": window.localStorage.getItem(AuthService.LOCAL_STORAGE_AUTH_TOKEN_KEY)
            };
            this.http.post(SERVER_URL + '/auth/check', data).subscribe(
                (response: any) => {
                    observer.next(true);
                    observer.complete();
                },
                (response: any) => {
                    observer.error(false);
                    observer.complete();
                }
            );
        });
    }

    register(user: User) {
        return new Observable((observer: Observer<any>) => {
            this.http.post(SERVER_URL + '/auth/registration', user)
                .map((response) => {
                    return response.json();
                })
                .subscribe(
                (data: any) => {
                    let token = data.token;
                    window.localStorage.setItem(AuthService.LOCAL_STORAGE_AUTH_TOKEN_KEY, token);
                    observer.next(token);
                    observer.complete();
                },
                (response: any) => {
                    observer.error(response.json().errors);
                    observer.complete();
                }
                );
        });
    }
}

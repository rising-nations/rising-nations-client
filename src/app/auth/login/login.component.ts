import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../common/auth.service';

@Component({
    selector: 'rn-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
    private errors: string[] = [];
    private username: string;
    private password: string;

    constructor(private auth: AuthService, private router: Router) {
    }

    ngOnInit() {
        let that = this;
        this.auth.checkLoginState().subscribe(
            () => {
                that.router.navigate(['/play']);
            }, () => {}
        );
    }

    doLogin() {
        let that = this;
        this.auth.login(this.username, this.password).subscribe(
            (jwt: string) => {
                that.router.navigate(['/play']);
            },
            (error: any) => {
                this.errors = error;
            }
        );
    }
}

import { User, AuthService } from '../../common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

const LOCAL_STORAGE_JWT_KEY = 'token';

@Component({
    selector: 'rn-registration',
    templateUrl: 'registration.component.html',
    styleUrls: ['registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    private errors: string[] = [];
    private user: User;
    private password_repetition: string;

    constructor(private auth: AuthService, private router: Router) {
        this.user = <User>{};
    }

    ngOnInit() {
        let that = this;
        this.auth.checkLoginState().subscribe(
            () => {
                that.router.navigate(['/play']);
            },
            () => { }
        );
    }

    doRegistration() {
        let that = this;
        if (this.password_repetition !== this.user.password) {
            this.errors[0] = 'Deine Passwörter müssen übereinstimmen!';
            return;
        }
        if (!this.user.agbAccepted) {
            this.errors[0] = 'Du musst die AGB akzeptieren!';
            return;
        }
        this.auth.register(this.user).subscribe(
            (token: string) => {
                that.router.navigate(['/play']);
            },
            (errors: any) => {
                that.errors = errors;
            }
        );
    }
}

import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'rising-nations-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  constructor() {
    // Do something with api
  }

  public ngOnInit() {
  }
}

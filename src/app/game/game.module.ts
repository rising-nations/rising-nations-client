import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ServerService } from './net';
import { MapComponent, MapService } from './map';

import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { GameComponent } from './game.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                pathMatch: 'full',
                component: GameComponent
            }
        ])
    ],
    declarations: [
        GameComponent,
        MapComponent
    ],
    exports: [
        RouterModule
    ],
    providers: [

        [ServerService],
        [MapService],
    ]
})
export class GameModule { }
import { Router } from '@angular/router';
import { AuthService } from '../../common';
import { Observable, Observer, Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

const GAME_ENDPOINT_URL = 'ws://localhost:8080/rising-nations-server/game';

@Injectable()
export class ServerService {
    private static RE = 0;

    private websocket: Subject<any>;
    private subjectList = {};

    private constructor(private auth : AuthService, private router : Router) {
        this.connect();
    }


    private connect() {
        this.websocket = Observable.webSocket({
            "url": GAME_ENDPOINT_URL + '?auth=' + this.auth.getSessionToken(),
            "protocol": "game",
            "resultSelector": this.resultSelector.bind(this)
        });
        this.websocket.subscribe(
            this.onMessage.bind(this),
            this.onError.bind(this),
            this.onComplete.bind(this)
        )
    }

    private resultSelector(event: MessageEvent): GameProtocolResponse {
        let data = JSON.parse(event.data);
        if(data.authFailed && data.authFailed === true){
            this.auth.clearSessionToken();
            this.router.navigate(['/auth/login']);
            throw new Error("authentification failed");
        }
        return <GameProtocolResponse> data;
    }

    public query(action: string, data: any): Subject<any> {
        console.log("query issued");
        var gameProtocolReq = {
            "id": this.generateUniqueRequestId(),
            "action": this.normalizeAction(action),
            "data": JSON.stringify(data) // FIXME encode the data in a string because on the server side, we don't know on first deserialize what Requet object should be instantiated.
        };
        this.subjectList[gameProtocolReq.id] = new Subject<any>();
        this.websocket.next(JSON.stringify(gameProtocolReq));
        return this.subjectList[gameProtocolReq.id];
    }

    private onComplete() {
        console.log("websocket completed");
    }

    private onError(err: any) {
        console.log("websocket was closed");
        console.log(err);
    }

    private onMessage(response: GameProtocolResponse) {
        let reqSubject = this.subjectList[response.id];
        if (response.successful) {
            reqSubject.next(response.data);
        } else {
            reqSubject.error(response.data);
        }
        reqSubject.complete();
        delete this.subjectList[response.id];
    }

    private generateUniqueRequestId() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    private normalizeAction(action: string) {
        return action.toLowerCase();
    }
}
interface GameProtocolResponse {
    id: string;
    successful: boolean;
    data: any;
}
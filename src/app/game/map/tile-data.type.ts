import { TerrainType } from './terrain-type.type';

export interface TileData {
    x: number;
    y: number;
    type: TerrainType;
}

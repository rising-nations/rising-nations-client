

export type TerrainType = 'DESERT' | 'FOREST' | 'GRASSLAND' | 'MOUNTAINS' | 'TUNDRA' | 'SEA';

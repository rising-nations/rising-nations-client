import { TileData } from './tile-data.type';
import { Observable, Observer } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ServerService } from '../net';

@Injectable()
export class MapService {
    private constructor(private server: ServerService) { }

    getCoordinatesAround(x: number, y: number): Observable<any> {
        // let payload = null;
        let data = {
            "x": x,
            "y": y
        };
        return this.server.query('load-tiles', data).map(
            (data: any) => {
                return <TileData[]>data;
            }
        );
    }
}

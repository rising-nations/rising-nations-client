// Set the scene size.
const VIEW_ANGLE = 90;
const NEAR = 0.2;
const FAR = 100;

export class LoadingAnimator {
    private size: number;
    private renderer: THREE.WebGLRenderer;
    private scene: THREE.Scene;
    private group: THREE.Group;
    private camera: THREE.PerspectiveCamera;
    private meshes: THREE.Mesh[];
    private currentMeshIndex = 0;

    private interval;

    public constructor(canvas: HTMLCanvasElement, size : number) {
        this.size = size;
        this.prepare(canvas);
    }

    prepare(canvas: HTMLCanvasElement) {
        // Create a WebGL renderer, camera and a scene
        this.renderer = new THREE.WebGLRenderer({
            'antialias': true, 
            'alpha': true,
            'canvas': canvas
        });
        this.renderer.setSize(this.size, this.size);
        this.renderer.setClearColor(new THREE.Color(0, 0, 0), 0);

        // Set some camera attributes.
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(
            VIEW_ANGLE,
            1,
            NEAR,
            FAR
        );
        this.camera.position.x = 0;
        this.camera.position.y = 0;
        this.camera.position.z = -50;
        this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        let material = new THREE.MeshBasicMaterial({
            color: 0x2c3e50,
            transparent: true,
            opacity: 0.2
        });


        let OUTER_RADIUS = this.size / 2.9;
        let templateHex = new THREE.CircleGeometry(OUTER_RADIUS, 6);

        let hexagonGeometry = new THREE.CircleGeometry(OUTER_RADIUS / 3, 6);

        this.group = new THREE.Group();
        this.meshes = [];
        for (let n = 0; n < 6; n++) {
            let mesh = new THREE.Mesh(hexagonGeometry, material.clone());
            mesh.position.z = 0;
            mesh.position.x = templateHex.vertices[n + 1].x;
            mesh.position.y = templateHex.vertices[n + 1].y;
            mesh.rotateX(Math.PI);
            this.group.add(mesh);
            this.meshes.push(mesh);
        }
        this.group.visible = false;
        this.scene.add(this.group);
        templateHex = null;
        this.reset();
    }

    start() {
        this.group.visible = true;
        this.interval = setInterval(this.intervalFunc, 20);
    }

    stop() {
        this.group.visible = false;
        this.reset();
        clearInterval(this.interval);
        this.renderer.render(this.scene, this.camera);
    }

    reset() {
        for (var n in this.meshes) {
            if (this.meshes[n]) {
                var mesh = this.meshes[n];
                mesh.material.opacity = 0.2;
                mesh.material.needsUpdate = true;
                mesh.scale.x = mesh.scale.y = 1;
            }
        }
        this.currentMeshIndex = 0;
        // last mesh needs +0.05 on scale, because it was never scaled up. and will be scaled down when animation starts
        this.meshes[this.meshes.length - 1].scale.x = this.meshes[this.meshes.length - 1].scale.y = 1.35;
        this.meshes[this.meshes.length - 1].material.opacity = 0.9;
    }

    intervalFunc = () => {
        let nextMesh = this.meshes[(this.currentMeshIndex + 1) === this.meshes.length ? 0 : this.currentMeshIndex + 1];
        let currentMesh = this.meshes[this.currentMeshIndex];
        let previousMesh = this.meshes[((this.currentMeshIndex - 1) < 0) ? this.meshes.length - 1 : this.currentMeshIndex - 1];


        nextMesh.material.opacity *= 1.2;
        nextMesh.scale.x = nextMesh.scale.y += 0.05;
        nextMesh.material.needsUpdate = true;
        currentMesh.material.opacity = 0.9;
        currentMesh.scale.x = currentMesh.scale.y = 1.35;
        currentMesh.material.needsUpdate = true;
        previousMesh.material.opacity *= 0.80;
        previousMesh.scale.x = previousMesh.scale.y -= 0.05;
        previousMesh.material.needsUpdate = true;

        if (previousMesh.material.opacity <= 0.2) {
            this.currentMeshIndex = (this.currentMeshIndex + 1 < this.meshes.length) ? (this.currentMeshIndex + 1) : 0;
        }
        window.requestAnimationFrame(() => {
            this.renderer.render(this.scene, this.camera);
        });
    }
}
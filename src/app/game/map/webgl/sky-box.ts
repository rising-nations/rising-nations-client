const TEXTURES = [
    'px.jpg',
    'nx.jpg',
    'py.jpg',
    'ny.jpg',
    'pz.jpg',
    'nz.jpg'
];

export class SkyBox {
    private aCubeMap;

    constructor() {
        this.init();
    }

    private init() {
        let loader = new THREE.CubeTextureLoader();
        loader.setPath('images/patterns/sky/');
        this.aCubeMap = loader.load(TEXTURES);
    }


    createSkyBox() {
        let mesh = new THREE.Mesh(
            new THREE.BoxBufferGeometry(10000, 10000, 10000),
            this.createMaterial()
        );

        return mesh;
    }

    createMaterial() {
        let aShader = THREE.ShaderLib['cube'];
        aShader.uniforms['tCube'].value = this.aCubeMap;
        let material = new THREE.ShaderMaterial({
            fragmentShader: aShader.fragmentShader,
            vertexShader: aShader.vertexShader,
            uniforms: aShader.uniforms,
            depthWrite: false,
            side: THREE.BackSide
        });
        return material;
    }
}

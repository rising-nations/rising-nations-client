import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';

const TEXTURES = {
    'DESERT': 'tiles/desert.jpg',
    'FOREST': 'tiles/forest.jpg',
    'GRASSLAND': 'tiles/grass.jpg',
    'TUNDRA': 'tiles/grass.jpg',
    'MOUNTAINS': 'tiles/grass.jpg',
    'SEA': 'tiles/water.jpg',

};

@Injectable()
export class TextureManager {

    private cache = [];
    private loader: THREE.TextureLoader;

    constructor() {
        this.loader = new THREE.TextureLoader();
        this.loader.setPath('images/patterns/');
    }

    public preload() {
        return this.preloadLoader();
    }

    private preloadLoader(): Observable<any> {
        let observables = []; // array with all observables
        for (let n in TEXTURES) {
            if (TEXTURES[n]) {
                // push observables
                observables.push(new Observable((observer: Observer<any>) => {
                    this.loader.load(TEXTURES[n], (texture) => {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.offset.y = 0.5;
                        texture.offset.y = 0.5;
                        texture.repeat.y = 0.1;
                        texture.repeat.x = 0.1;
                        this.cache[n] = texture;
                        observer.next(texture);
                        observer.complete();
                    });
                }));
            }
        }
        // runs all observables in parallel and emits next and complete if the last is finished
        return Observable.forkJoin(observables);
    }
    public getTexture(name: string): THREE.Texture {
        return <THREE.Texture>this.cache[name];
    }
}

import { TerrainType } from '../terrain-type.type';
import { TextureManager } from './texture-manager';

const TILE_FRAGMENT_SHADER = require('./shaders/tile.fragment.glsl');

export class HexTile {

    public static SIDE = 15; // the hexagon side length in pixels
    public static GAP = 0; // Gap in px between hexagons
    public static HALF_SIDE = 0.5 * HexTile.SIDE; // half side (used for calculations)
    public static HALF_HEIGHT = Math.sqrt((HexTile.SIDE + HexTile.HALF_SIDE) * (HexTile.SIDE - HexTile.HALF_SIDE)); // half height (used for calculations)

    private geometry: THREE.Geometry;

    // Cache
    private materialCache: THREE.Material[] = [];

    constructor(private renderer: THREE.WebGLRenderer, private camera: THREE.Camera, private scene: THREE.Scene, private textureMgr: TextureManager) {
        this.init();
    }

    private init() {
        this.geometry = this.createHexTileGeometry();
    }

    public createNewMesh(x: number, y: number, type: TerrainType) {
        // Geometry can be cache
        let mesh = new THREE.Mesh(this.geometry, this.createHexTileMaterial(type, x, y));
        [mesh.position.x, mesh.position.y, mesh.position.z] = this.calcCoordinates(x, y);
        mesh.rotateX(Math.PI / -2);
        mesh.userData['type'] = type;
        mesh.userData['x'] = x;
        mesh.userData['y'] = y;
        return mesh;
    }

    public calcCoordinates(x: number, y: number) {
        if (y % 2 === 0) {
            return [
                ((HexTile.SIDE + HexTile.GAP) * 2 * x) + ((HexTile.SIDE + HexTile.GAP) * x),
                0, // y
                (y / 2) * (Math.sqrt((HexTile.SIDE + HexTile.GAP) * (HexTile.SIDE + HexTile.GAP) - ((HexTile.SIDE + HexTile.GAP) / 2) * ((HexTile.SIDE + HexTile.GAP) / 2))) * 2
            ];
        } else {
            return [
                ((HexTile.SIDE + HexTile.GAP) * 2 * x) + ((HexTile.SIDE + HexTile.GAP) * x) + (HexTile.SIDE + HexTile.GAP) * 1.5,
                0, // y
                (y) * (Math.sqrt((HexTile.SIDE + HexTile.GAP) * (HexTile.SIDE + HexTile.GAP) - ((HexTile.SIDE + HexTile.GAP) / 2) * ((HexTile.SIDE + HexTile.GAP) / 2)))
            ];
        }
    }

    public createHexTileGeometry() {
        let geometry = new THREE.CircleGeometry(HexTile.SIDE, 6);
        return geometry;
    }

    public createHexTileMaterial(type: TerrainType, x: number, y : number) {
        if (this.materialCache[type]) {
            return this.materialCache[type];
        }
        // front material (top of hexagon)
        let frontMaterial = this.createFrontMaterial(type, x, y);
        // Side material (bevel and side)
        // let sideMaterial = this.createSideMaterial();
        // let materials = <THREE.Material[]>[frontMaterial, sideMaterial];
        // let material = new THREE.MultiMaterial(materials);
        this.materialCache[type] = frontMaterial;
        return frontMaterial;
    }

    protected createFrontMaterial(type: TerrainType, x: number, y: number) {
        let texture = this.textureMgr.getTexture(type);
        let material = new THREE.MeshBasicMaterial({
            map: texture,
            
        });
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.x = texture.repeat.y = 0.9;
        /*
        let shaderMaterial = new THREE.ShaderMaterial({
            uniforms: THREE.UniformsUtils.merge([
                THREE.UniformsLib['lights'],
                {
                    'tilePos': { value: new THREE.Vector2() },
                    'scale': { type: "f", value: 1.0 }
                }
            ]),
            fragmentShader: TILE_FRAGMENT_SHADER,
            transparent: false,
            wireframe: false
        });
        //shaderMaterial.uniforms['u_resolution'].value.x = this.renderer.domElement.width;
        //shaderMaterial.uniforms['u_resolution'].value.y = this.renderer.domElement.height;
        shaderMaterial.uniforms['tilePos'].value.x = x;
        shaderMaterial.uniforms['tilePos'].value.y = y;
        shaderMaterial.uniforms['scale'].value = this.camera.scale;
        */
        return material;
    }

    protected createSideMaterial() {
        let side = new THREE.MeshLambertMaterial({
            color: 0xEFF0F1
        });
        return side;
    }

}

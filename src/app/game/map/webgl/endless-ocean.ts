import { TextureManager } from './texture-manager';
export class EndlessOcean {
    constructor(private textureMgr: TextureManager) {
    }
    create() {
        let mesh = new THREE.Mesh(
            new THREE.PlaneGeometry(1000000, 1000000),

            this.createMaterial()
        );
        mesh.rotateX(Math.PI / -2);

        mesh.position.x = mesh.position.z = 0
        mesh.position.y = -0.1;
        return mesh;
    }

    createMaterial() {
        let texture = this.textureMgr.getTexture('SEA');
        let material = new THREE.MeshBasicMaterial({
            map: texture,

        });
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        return material;
    }
}

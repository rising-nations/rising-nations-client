import { TextureManager } from './texture-manager';
import { TerrainType } from '../';
import { TileData } from '../tile-data.type';

import { HexTile } from './hex-tile';

export class HexGrid {
    public static SELECTOR_WIDTH = 0.5;

    private tile: HexTile;
    public group: THREE.Group;
    private selectedTile: THREE.Mesh;

    private selectorMesh: THREE.Mesh;

    constructor(private renderer: THREE.WebGLRenderer, private camera: THREE.Camera, private scene: THREE.Scene, private textureMgr: TextureManager) {
        this.tile = new HexTile(this.renderer, this.camera, this.scene, this.textureMgr);
        this.group = new THREE.Group();

        this.scene.add(this.group);
        this.initalizeSelectorMesh();
    }

    protected initalizeSelectorMesh() {
        let geometry = new THREE.RingGeometry(HexTile.SIDE - HexGrid.SELECTOR_WIDTH, HexTile.SIDE, 6);
        let selectorMaterial = new THREE.MeshBasicMaterial({ color: 0xfff000, side: THREE.DoubleSide });
        this.selectorMesh = new THREE.Mesh(geometry, selectorMaterial);
        this.selectorMesh.rotateX(Math.PI / -2);
        this.selectorMesh.visible = false;
        this.scene.add(this.selectorMesh);
    }

    public focusTile(tileX : number, tileY : number){
        let [x, y, z] = this.tile.calcCoordinates(tileX, tileY);
        this.selectorMesh.position.x = x;
        this.selectorMesh.position.z = z;
        this.selectorMesh.visible = true;
        return [x,y,z];
    }

    public clear() {
        this.scene.remove(this.group);
        this.group = new THREE.Group();
    }

    public drawMap = (data: TileData[]) => {
        let counter = 1;
        data.forEach((tileData: TileData) => {
                this.group.add(this.tile.createNewMesh(tileData.x, tileData.y, tileData.type));
        });
    }

    public onClicked = (clicked: THREE.Mesh) => {
        let userdata = clicked.userData;
        let previousSelected = this.selectedTile;
        if (userdata.hasOwnProperty('x') && userdata.hasOwnProperty('y') && userdata.hasOwnProperty('type')) { // Only hex tiles have this attributes.
            if (previousSelected) {
                previousSelected.material = this.tile.createHexTileMaterial(<TerrainType>previousSelected.userData['type'], userdata['x'], userdata['y']);
            }
            //clicked.material = new THREE.MeshBasicMaterial({ color: 0xC65591 });

            this.selectorMesh.position.x = clicked.position.x;
            this.selectorMesh.position.y = clicked.position.y + 0.01;
            this.selectorMesh.position.z = clicked.position.z;
            this.selectorMesh.visible = true;
            this.selectedTile = clicked;
        }
    }
}

import { EndlessOcean } from './endless-ocean';
import { TextureManager } from './texture-manager';
import { TileData } from '../tile-data.type';
import { SkyBox } from './sky-box';
import { HexGrid } from './hex-grid';
import { Observable, Observer } from 'rxjs/Rx';

require('./OrbitControls');

export class RisingNationsMap {

    private renderer: THREE.WebGLRenderer;
    private camera: THREE.PerspectiveCamera;
    private scene: THREE.Scene;
    private controls: THREE.OrbitControls;
    private hexGrid: HexGrid;
    private raycaster: THREE.Raycaster;
    private currentPosition: THREE.Vector2;

    private textureMgr: TextureManager;

    constructor(private width: number, private height: number) {
    }

    /*
     * Prepares the map. Initalizes all models.
     * Returns a Observable, which is completed when preparing is finished.
     */
    public prepare(canvas: HTMLCanvasElement): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.textureMgr = new TextureManager();
            this.preloadTextures().subscribe(
                () => {
                    observer.next(1);
                    this.addEnvironment();
                    observer.complete();
                }
            );
            this.renderer = this.createRenderer(canvas);
            this.camera = this.createCamera();
            this.controls = this.createControls(this.camera);
            this.scene = this.createScene();
            this.hexGrid = this.createHexGrid(this.renderer, this.camera, this.scene, this.textureMgr);
            this.controls.addEventListener('change', this.onControlsChange);

            // Add Objects
            this.addLights();
            this.raycaster = new THREE.Raycaster();
            this.renderer.domElement.addEventListener('mousedown', this.onCanvasMouseDown, false);
        });
    }

    protected preloadTextures() {
        return this.textureMgr.preload();
    }

    protected onControlsChange = () => {
        let screenMiddle = new THREE.Vector3(
            (this.width * 0.5 / this.width) * 2 - 1,
            - (this.height * 0.5 / this.height) * 2 + 1,
            0
        );
        this.raycaster.setFromCamera(screenMiddle, this.camera);
        let intersections = this.raycaster.intersectObjects(this.hexGrid.group.children);
        if (intersections.length <= 0) {
            window.console.log('User navigated out of the Map!!');
        } else {
            let userData = intersections[0].object.userData;
            this.currentPosition = new THREE.Vector2(userData['x'], userData['y']);
        }
        this.render();
    }

    protected onCanvasMouseDown = (event) => {

        event.preventDefault();

        let vector = new THREE.Vector3(
            (event.clientX / this.width) * 2 - 1,
            - (event.clientY / this.height) * 2 + 1,
            0
        );

        this.raycaster.setFromCamera(vector, this.camera);

        let intersections = this.raycaster.intersectObjects(this.hexGrid.group.children);
        if (intersections.length > 0) {
            this.hexGrid.onClicked(<THREE.Mesh>intersections[0].object);
        }
        this.render();
    }

    protected createRenderer(canvas: HTMLCanvasElement) {
        let renderer = new THREE.WebGLRenderer({
            'canvas': canvas,
            'antialias': true
        });
        renderer.setSize(this.width, this.height);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        return renderer;
    }

    protected createCamera() {
        const camera = new THREE.PerspectiveCamera(
            20,  // FOV
            this.width / this.height,  // aspect
            1,   // NEAR
            20000 // FAR
        );
        camera.position.y = 500;
        camera.position.z = 0;
        camera.position.x = 0;
        return camera;
    }

    protected createControls(camera: THREE.PerspectiveCamera) {
        let controls = new THREE.OrbitControls(camera);
        controls.minDistance = 400;
        controls.maxDistance = 700;
        //controls.target = new THREE.Vector3(0, 0, 0);
        controls.enableZoom = true;
        controls.enableRotate = false;
        controls.mouseButtons = { ORBIT: THREE.MOUSE.RIGHT, ZOOM: THREE.MOUSE.RIGHT, PAN: THREE.MOUSE.LEFT };
        return controls;
    }

    protected createScene() {
        let scene = new THREE.Scene();
        scene.fog = new THREE.FogExp2(0xcccccc, 0.0005);
        return scene;
    }

    protected createHexGrid(renderer: THREE.WebGLRenderer, camera: THREE.Camera, scene: THREE.Scene, textureMgr: TextureManager) {
        let grid = new HexGrid(renderer, camera, scene, textureMgr);
        return grid;
    }

    public focusTile(tileX, tileY) {
        let [x, y, z] = this.hexGrid.focusTile(tileX, tileY);
        this.camera.position.z = z;
        this.camera.position.x = x;
        this.camera.lookAt(new THREE.Vector3(x, y, z));
        this.controls.target = new THREE.Vector3(x, y, z);
        this.render();
    }

    protected addLights() {
        // create a point light
        const pointLight = new THREE.PointLight(0xFFFFFF);

        // set its position
        pointLight.position.x = 110;
        pointLight.position.y = 550;
        pointLight.position.z = 130;
        this.scene.add(pointLight);
    }

    protected addEnvironment() {
        let ocean = new EndlessOcean(this.textureMgr);
        this.scene.add(ocean.create());
    }

    public render = () => {
        // Draw!
        this.renderer.render(this.scene, this.camera);
    }

    public startRenderLoop() {
        let that = this;
        window.requestAnimationFrame(this.renderLoop.bind(that));
    }

    private renderLoop = () => {
        // update controls
        this.controls.update();
        // render
        this.render();
        // Schedule the next frame.
        window.requestAnimationFrame(this.renderLoop);
    }

    public drawMap(data: TileData[]) {
        let before = Date.now();
        this.hexGrid.drawMap(data);
        this.render();
        console.log("Map drawing took " + (Date.now() - before) + "ms");
    }
}

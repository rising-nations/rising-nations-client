import { LoadingAnimator } from './webgl/loading-animator';
import { TextureManager } from './webgl/texture-manager';
import { TileData } from './tile-data.type';
import { RisingNationsMap } from './webgl';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { MapService } from './map.service';

interface MapControls {
    x?: number;
    y?: number;
}

@Component({
    selector: 'rn-map',
    templateUrl: 'map.component.html',
    providers: [
        [TextureManager]
    ],
    styleUrls: ['map.component.scss']
})
export class MapComponent implements OnInit {

    private map: RisingNationsMap;
    private sound: HTMLAudioElement;

    private controls: MapControls = {};

    @ViewChild('mapCanvas')
    mapCanvas: ElementRef;

    @ViewChild('loaderCanvas')
    loaderCanvas: ElementRef;

    constructor(private mapService: MapService) {
        // Do something with api
    }

    ngOnInit() {
        let loader = new LoadingAnimator(<HTMLCanvasElement>this.loaderCanvas.nativeElement, 100);
        loader.start();
        this.map = new RisingNationsMap(window.innerWidth, window.innerHeight);
        let that = this;
        this.map.prepare(<HTMLCanvasElement>this.mapCanvas.nativeElement).subscribe(
            () => {
                window.console.log('Map finished preparing!');
                loader.stop();
                that.updateMap(0, 0);
            },
            (error: any) => {
                window.console.error('Failed to prepare map: ' + error);
                loader.stop();
            }
        );

        this.initSound();
    }

    initSound() {
        this.sound = <HTMLAudioElement>document.createElement('audio');
        this.sound.controls = false;
        if (this.sound.canPlayType('audio/mp3') !== '') {
            this.sound.src = 'music/bensound-epic.mp3';
            this.sound.loop = true;
            // this.sound.play();
        }
    }

    toggleSound() {
        if (!this.sound.paused) {
            this.sound.pause();
        } else {
            this.sound.play();
        }
    }

    onMapControlsSubmit() {
        this.updateMap(this.controls.x, this.controls.y);
        this.map.focusTile(this.controls.x, this.controls.y);
    }

    updateMap(x: number, y: number) {
        let that = this;
        this.mapService.getCoordinatesAround(x, y).subscribe(
            (data: TileData[]) => {
                that.map.drawMap(data);
            },
            (error: any) => {
                console.log(error);
                alert('Error loading map');
            }
        );
    }
}

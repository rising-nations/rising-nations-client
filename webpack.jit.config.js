'use strict';
let path = require('path');

module.exports = {
  entry: require('./webpack/entry'),

  context: path.join(process.cwd(), 'src'),

  output: {  
    path: path.join(process.cwd(), 'build-jit'),
    filename: '[name].bundle.js'
  },

  module: require('./webpack/module.jit'),

  plugins: require('./webpack/plugins'),

  resolve: require('./webpack/resolve'),

  devServer: require('./webpack/dev-server'),

  stats: 'errors-only',

  devtool: 'source-map'
};
